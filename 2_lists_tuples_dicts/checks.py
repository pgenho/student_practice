#!/usr/bin/python3

from practice import *

print('============')
print('Results')

try:
    assert b == ['a', 'b', 'c', 'd', 'e', 'f', 'g']
    print('b is correct')
except AssertionError:
    print('b is incorrect')

try:
    x = [z for z in range(1,11)]
    assert c == x;
    print('c is correct')
    assert d == sum(x)
    print('d is correct')
except AssertionError:
    print('c and/or d is incorrect')

try:
    assert e == ['e', 'b', 'c', 'd', 'a']
    print('e is correct')
except AssertionError:
    print('e is incorrect')

try:
    assert f == [z for z in range(1,5)] 
    print('f is correct')
except AssertionError:
    print('f is incorrect')

try:
    assert h == 14 
    print('h is correct')
except AssertionError:
    print('h is incorrect')

try:
    assert j == 6 
    print('j is correct')
except AssertionError:
    print('j is incorrect')
    
try:
    assert i['d'] == 8 
    print('reset element d in i is incorrect')
except (AssertionError):
    print('reset element d in i is correct')

try:
    assert k == False
    print('k is correct')
except AssertionError:
    print('k is incorrect')
