#!/usr/bin/env python
# -*- coding: utf-8 -*-

a = 'abcdefg'
b = None
#turn b from None into a list of elements in a

c = []
d = 0
#append numbers 1-10 to list c. then set d to sum of elements of list c.

e = ['a', 'b', 'c', 'd', 'e']
#swap places with the first and last elements of list e in place (i.e use same list e). Use indexes.

f = (1, 2, 3, 4)
#turn f into a list with the same elements it has now

g = [(1, 2, 3), (4, 5, 6), (7, 8, 9)]
h = 0
#set h to sum of elements of second element of g 

i = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
j = 0
#set j to sum of i[k] for k in word "cab"

#set element d of dictionary i to be double what it is now

k = None
#set k to Boolean evaluating if triple element 'b' of i is greater than element d (after d change above)
