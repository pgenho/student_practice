#!/usr/bin/python3

'''Practice for basic types and operations.'''

a = 0
#Set variable a to the sum of 27 + 35 + 123
a = 27 + 35 + 123

b = 0
#Set variable b to 6327 - 52

c = True
#Set variable c to Boolean(a is greater than b)

d = '' 
#Set variable d to first name + last name (with space in between, caps matter)

e = 0
#Set variable e to the remainder of b divided by a

f = 0
#Set variable f to (a divided by e (no remainder)) + (a divided by e (with remainder)) 

g = True 
#Set variable g to True if a is equal to e

h = 'Mary'
i = ''
j = ''
#Set variable h to the third letter of i 

#set variable j to the first letter of i + last letter of i  << all lowercase






