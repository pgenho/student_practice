#!/usr/bin/python3

from practice import *

print('============')
print('Results')

try:
    assert a == sum([27, 35, 123])  
    print('a is correct')
except AssertionError:
    print('a is incorrect')

try:
    assert b == 6275 
    print('b is correct')
except AssertionError:
    print('b is incorrect')

try:
    assert c is False 
    print('c is correct')
except AssertionError:
    print('c is incorrect')

try:
    assert d == 'Grace Shea' 
    print('d is correct')
except AssertionError:
    print('d is incorrect')

try:
    assert d == 'Grace Shea' 
    print('d is correct')
except AssertionError:
    print('d is incorrect')

try:
    assert e == 6275 % sum([27, 35, 123])
    print('e is correct')
except AssertionError:
    print('e is incorrect')
    
try:
    xx = sum([27, 35, 123])
    assert f == (xx // e ) + (xx / e)
    print('f is correct')
except (AssertionError, ZeroDivisionError):
    print('f is incorrect')

try:
    assert not g 
    print('g is correct')
except AssertionError:
    print('g is incorrect')

try:
    assert i == 'r'
    print('i is correct')
except AssertionError:
    print('i is incorrect')

try:
    assert j == 'my'
    print('j is correct')
except AssertionError:
    print('j is incorrect')
